﻿module Desugar_Typed

open Expr_Typed
open Expr

exception Desugar_Error_Typed of string

let rec desugarT (expr: exprST) : exprT =
    match expr with
    | NumST n -> NumT n
    | BoolST b -> BoolT b
    | StringST s -> StringT s
    | IdST s -> IdT s
    | EmptyST t -> EmptyT t
    | UOpST (op, fst) -> desugar_unaryT op fst
    | BinopST (op, fst, snd) -> desugar_binary_opT op fst snd
    | BinopST' (op, args) -> desugar_binary_op_var_argT op args
    | CondST (cases) -> desugarT_cond cases
    | IfST (cond, then', else') -> IfT(desugarT cond, desugarT then', desugarT else')
    | Define_ConstST (id, typ, expr) -> Define_ConstT(id, typ, desugarT expr)
    | Fun_DefST (name, params', param_types, ret_type, body) ->
        DefineT(name, LambdaT(params', param_types, ret_type, desugarT body))
    | LambdaST (param, param_t, ret_t, body) -> LambdaT(param, param_t, ret_t, desugarT body)
    | Fun_AppST (f, x) -> Fun_AppT(desugarT f, List.map (fun a -> desugarT a) x)

and desugar_unaryT op fst =
    match op with
    | DecS -> BinopT(Add, NumT(1), desugarT fst)
    | IncS -> BinopT(Add, NumT(-1), desugarT fst)
    | NotS -> IfT(desugarT fst, BoolT false, BoolT true)
    | FirstS -> UOpT(First, desugarT fst)
    | RestS -> UOpT(Rest, desugarT fst)
    | IsEmptyS -> UOpT(IsEmpty, desugarT fst)

and desugar_binary_op_var_argT op args =
    match op with
    | MulS -> var_arg_binop_to_nestedT Mul args (NumT 1)
    | AddS -> var_arg_binop_to_nestedT Add args (NumT 0)
    | StrAppendS -> var_arg_binop_to_nestedT StrAppend args (StringT "")

and var_arg_binop_to_nestedT op args baseval =
    match args with
    | [] -> baseval
    | x :: [] -> desugarT x
    | first :: rest -> BinopT(op, desugarT first, var_arg_binop_to_nestedT op rest baseval)

and desugar_binary_opT op fst snd =
    match op with
    | LinkS -> BinopT(Link, desugarT fst, desugarT snd)
    | SubS -> BinopT(Sub, desugarT fst, desugarT snd)
    | DivS -> BinopT(Div, desugarT fst, desugarT snd)
    | ModS -> BinopT(Mod, desugarT fst, desugarT snd)
    | EQS -> BinopT(EQ, desugarT fst, desugarT snd)
    | GTS -> BinopT(GT, desugarT fst, desugarT snd)
    | LTS -> BinopT(LT, desugarT fst, desugarT snd)
    // Short circuit booleans
    | AndS -> IfT(desugarT fst, desugarT snd, BoolT false)
    | OrS -> IfT(desugarT fst, BoolT true, desugarT snd)

/// Desugar cond cases into nested if then elses
and desugarT_cond cases =
    match cases with
    | [] -> raise (Desugar_Error_Typed "Unable to desugar cond.")
    | [ IdST "else", body ] -> desugarT body
    | (test, body) :: rest -> IfT(desugarT test, desugarT body, desugarT_cond rest)
