﻿module Desugarer

open Expr

exception Desugar_Error of string

let rec desugar (expr: exprS) : expr =
    match expr with
    | NumS n -> NumC n
    | BoolS b -> BoolC b
    | StringS s -> StringC s
    | IdS s -> IdC s
    | EmptyS -> EmptyC
    | UOpS (op, fst) -> desugar_unary op fst
    | BinopS (op, fst, snd) -> desugar_binary_op op fst snd
    | BinopS' (op, args) -> desugar_binary_op_var_arg op args
    | CondS cases -> desugar_cond cases
    | LetS (bindings, body) ->
        FunAppC(LambdaC(get_let_ids bindings, desugar body), List.map desugar (get_let_exprs bindings))
    | IfS (cond, then', else') -> IfC(desugar cond, desugar then', desugar else')
    | DefineS (id, expr) -> DefineC(id, desugar expr)
    | Fun_DefS (f, params', body) -> DefineC(f, LambdaC(params', desugar body))
    | LambdaS (params', body) -> LambdaC(params', desugar body)
    | Fun_AppS (f, args) -> FunAppC(desugar f, List.map desugar args)
    | PrintS (args) -> Print(List.map desugar args)


and desugar_unary op fst =
    match op with
    | DecS -> BinopC(Add, NumC(1), desugar fst)
    | IncS -> BinopC(Add, NumC(-1), desugar fst)
    | NotS -> IfC(desugar fst, BoolC false, BoolC true)
    | FirstS -> UOpC(First, desugar fst)
    | RestS -> UOpC(Rest, desugar fst)
    | IsEmptyS -> UOpC(IsEmpty, desugar fst)

and desugar_binary_op_var_arg op args =
    match op with
    | MulS -> var_arg_binop_to_nested Mul args (NumC 1)
    | AddS -> var_arg_binop_to_nested Add args (NumC 0)
    | StrAppendS -> var_arg_binop_to_nested StrAppend args (StringC "")

and var_arg_binop_to_nested op args baseval =
    match args with
    | [] -> baseval
    | x :: [] -> desugar x
    | first :: rest -> BinopC(op, desugar first, var_arg_binop_to_nested op rest baseval)

and get_let_ids (l: (id * exprS) list) = List.map fst l

and get_let_exprs (l: (id * exprS) list) = List.map snd l

and desugar_binary_op op fst snd =
    match op with
    | LinkS -> BinopC(Link, desugar fst, desugar snd)
    | SubS -> BinopC(Sub, desugar fst, desugar snd)
    | DivS -> BinopC(Div, desugar fst, desugar snd)
    | ModS -> BinopC(Mod, desugar fst, desugar snd)
    | EQS -> BinopC(EQ, desugar fst, desugar snd)
    | GTS -> BinopC(GT, desugar fst, desugar snd)
    | LTS -> BinopC(LT, desugar fst, desugar snd)
    // Short circuit booleans
    | AndS -> IfC(desugar fst, desugar snd, BoolC false)
    | OrS -> IfC(desugar fst, BoolC true, desugar snd)

/// Desugars cond cases into nested if then elses
and desugar_cond cases =
    match cases with
    | [] -> raise (Desugar_Error("Unable to desugar cond."))
    | [ IdS "else", body ] -> desugar body
    | (test, body) :: rest -> IfC(desugar test, desugar body, desugar_cond rest)
