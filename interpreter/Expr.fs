﻿module Expr 

/// Sugar unary operators
type unary_opS =
    | IncS
    | DecS
    | NotS
    | FirstS
    | RestS
    | IsEmptyS

/// Sugar binary operators
type binary_opS =
    | EQS
    | GTS
    | LTS
    | GTES
    | LTES
    | AddS
    | MulS
    | SubS
    | DivS
    | AndS
    | OrS
    | ModS
    | StrAppendS
    | LinkS

/// Core Unary operators
type unary_op =
    | First
    | Rest
    | IsEmpty

/// Core binary operators
type binary_op =
    | EQ
    | GT
    | LT
    | Add
    | Mul
    | Div
    | Sub
    | Mod
    | Link
    | StrAppend

type name = string
type parameters = string list
type id = string

type body = expr
and bodyS = exprS

/// Sugar syntax
and exprS =
    | NumS of int
    | BoolS of bool
    | StringS of string
    | IdS of string
    | UOpS of unary_opS * exprS
    | CondS of (exprS * exprS) list
    | LetS of (id * exprS) list * bodyS
    | IfS of exprS * exprS * exprS
    | BinopS of binary_opS * exprS * exprS
    /// Variable # of args, eg: (+ 1 2 3 4)
    | BinopS' of binary_opS * exprS list 
    | DefineS of name * exprS
    | Fun_DefS of name * parameters * bodyS
    | Fun_AppS of exprS * exprS list
    | LambdaS of parameters * bodyS
    | PrintS of exprS list
    | EmptyS

/// Core language    
and expr =
    | NumC of int
    | BoolC of bool
    | StringC of string
    | IdC of string
    | UOpC of unary_op * expr
    | IfC of expr * expr * expr
    | BinopC of binary_op * expr * expr
    | DefineC of name * expr
    | FunAppC of expr * expr list
    | LambdaC of parameters * body
    | Print of expr list
    | EmptyC

type value =
    | NumV of int
    | BoolV of bool
    | ClosureV of parameters * body * venv
    | StringV of string
    /// Cons
    | LinkV of value * value
    | EmptyV
    /// For returning definitions to the REPL
    | Venv of defs * id * value

/// Value Environment
and venv = Map<string, value>
/// Top level function definitions
and defs = Map<string, value>

