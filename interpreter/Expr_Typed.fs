﻿module Expr_Typed

open Expr

type Type =
    | Num
    | Bool
    | String
    | Fun of param_types * return_type
    | List of Type
    /// For returning new definitions to the repl.
    | Tenv of tenv * id * Type 

and param_types = Type list
and return_type = Type

and bodyST = exprST
and bodyT = exprT

and parameter = string
and parameters = string list

/// Sugar Syntax Typed
and exprST =
    | NumST of int
    | BoolST of bool
    | StringST of string
    | EmptyST of Type
    | IdST of string
    | UOpST of unary_opS * exprST
    | CondST of (exprST * exprST) list
    | IfST of exprST * exprST * exprST
    | BinopST of binary_opS * exprST * exprST
    | BinopST' of binary_opS * exprST list
    | Define_ConstST of name * Type * exprST
    | Fun_DefST of name * parameters * param_types * return_type * bodyST
    | Fun_AppST of exprST * exprST list
    | LambdaST of parameters * param_types * return_type * bodyST

/// Core Typed
and exprT =
    | NumT of int
    | BoolT of bool
    | StringT of string
    | EmptyT of Type
    | IdT of string
    | UOpT of unary_op * exprT
    | IfT of exprT * exprT * exprT
    | BinopT of binary_op * exprT * exprT
    | Define_ConstT of name * Type * exprT
    | DefineT of name * exprT
    | Fun_AppT of exprT * exprT list
    | LambdaT of parameters * param_types * return_type * bodyT

/// Type Environment
and tenv = Map<string, Type>
