﻿module Interp

// Resources: http://cs.brown.edu/courses/cs173/2012/book/

open Expr

exception Interp_Binop_Error of binary_op * value * value
exception Interp_Binop_Error_Div of value * value
exception Interp_Unary_Error of unary_op * value
exception Interp_Unbound_Id of string
exception Interp_If_Then_Error of string
exception Interp_Fun_App_Error of string


let binop_value op f (l: value) (r: value) =
    match l, r with
    | NumV a, NumV b -> (NumV(f a b))
    | v1, v2 -> raise (Interp_Binop_Error(op, v1, v2))

let num_add l r = binop_value Add (+) l r
let num_mul l r = binop_value Mul (*) l r
let num_sub l r = binop_value Sub (-) l r
let num_div l r = binop_value Div (/) l r
let num_mod l r = binop_value Mod (%) l r

let num_compare f (l: value) (r: value) : value =
    match l, r with
    | NumV a, NumV b when f a b -> BoolV true
    | NumV a, NumV b when not (f a b) -> BoolV false

let string_compare f (l: value) (r: value) : value =
    match l, r with
    | StringV a, StringV b when f a b -> BoolV true
    | StringV a, StringV b when not (f a b) -> BoolV false

/// Overload string and num compare operators
let val_compare op (l: value) (r: value) : value =
    match op, l, r with
    | EQ, NumV _, NumV _ -> num_compare (=) l r
    | EQ, StringV s, StringV t -> string_compare (=) l r
    | GT, NumV a, NumV b -> num_compare (>) l r
    | GT, StringV s, StringV t -> string_compare (>) l r
    | LT, NumV a, NumV b -> num_compare (>) l r
    | LT, StringV s, StringV t -> string_compare (>) l r
    | op, v1, v2 -> raise (Interp_Binop_Error(op, v1, v2))

let print_val v =
    match v with
    | StringV s -> printf "%s" s
    | NumV s -> printf "%d" s
    | BoolV b -> printf "%b" b
    | _ -> raise (Interp_Fun_App_Error "Unable to print value")

let str_append l r =
    match l, r with
    | StringV a, StringV b -> StringV(a + b)
    | v1, v2 -> raise (Interp_Binop_Error(StrAppend, v1, v2))

let is_empty l =
    match l with
    | EmptyV -> BoolV true
    | LinkV (_, _) -> BoolV false
    | _ -> raise (Interp_Unary_Error(IsEmpty, l))

let get_rest l =
    match l with
    | LinkV (_, r) -> r
    | _ -> raise (Interp_Unary_Error(Rest, l))

let get_first l =
    match l with
    | LinkV (f, _) -> f
    | _ -> raise (Interp_Unary_Error(First, l))

let mt_venv: venv = Map.empty<string, value>

/// Try to look up in the top level definitions, but prioritise bindings in current local scope environment.
let rec look_up (x: string) (env: venv) (defs: defs) : value =
    match defs.TryFind(x), env.TryFind(x) with
    | None, None -> raise (Interp_Unbound_Id x)
    | None, Some s -> s
    | Some s, None -> s
    | Some _, Some a -> a

/// Always call the interpreter with an empty environment.
and interpret expr defs = interp expr mt_venv defs

/// Big-step evaluator function.
and interp (expr: expr) (env: venv) (defs: defs) : value =
    match expr with
    | NumC n -> NumV n
    | BoolC b -> BoolV b
    | StringC s -> StringV s
    | EmptyC -> EmptyV
    | IdC s -> look_up s env defs
    | UOpC (uop, fst) -> interp_unary uop fst env defs
    | BinopC (bop, fst, snd) -> interp_binary bop fst snd env defs
    | IfC (cond, then', else') -> interp_if cond then' else' env defs
    | DefineC (id, expr) ->
        let v = interp expr env defs
        Venv(defs.Add(id, v), id, v)
    | LambdaC (params', body) -> ClosureV(params', body, env)
    | FunAppC (f, args) -> interp_fun_variable_args f args env defs
    | Print args -> print_args args env defs

and interp_unary uop expr env defs =
    match uop, interp expr env defs with
    | First, l -> get_first l
    | Rest, l -> get_rest l
    | IsEmpty, l -> is_empty l

and interp_binary (bop: binary_op) (expr1: expr) (expr2: expr) (env: venv) defs : value =
    match bop, interp expr1 env defs, interp expr2 env defs with
    | Add, fst, snd -> num_add fst snd
    | Sub, fst, snd -> num_sub fst snd
    | Mul, fst, snd -> num_mul fst snd
    | Div, fst, NumV 0 -> raise (Interp_Binop_Error_Div(fst, NumV 0))
    | Div, fst, snd -> num_div fst snd
    | Mod, fst, snd -> num_mod fst snd
    | EQ, fst, snd -> val_compare EQ fst snd
    | GT, fst, snd -> val_compare GT fst snd
    | LT, fst, snd -> val_compare LT fst snd
    | StrAppend, fst, snd -> str_append fst snd
    | Link, fst, snd -> LinkV(fst, snd)

and interp_if cond then' else' env defs =
    match interp cond env defs with
    | BoolV true -> interp then' env defs
    | BoolV false -> interp else' env defs
    | s -> raise (Interp_If_Then_Error "If condition error: expected bool")

and interp_fun_variable_args f args env defs =
    match interp f env defs with
    | ClosureV (ps, _, _) when ps.Length <> args.Length -> raise (Interp_Fun_App_Error "Wrong # of arguments")
    | ClosureV (ps, body, cl_env) -> interp body (bind_params ps args env cl_env defs) defs
    | _ -> raise (Interp_Fun_App_Error "Unable to interpret function application.")

/// Evaluate all the arguments to the function, bind them to the corresponding parameters,
/// and return the new value environment. The closure environment accumulates all bindings.
and bind_params (parameters: string list) (args: expr list) env (closurevenv: venv) defs =
    List.fold2 (fun acc a b -> acc.Add(a, interp b env defs)) closurevenv parameters args

/// Just return a string for now. No unit type was implemented.
and print_args args env defs =
    let _ =
        List.map (fun x -> print_val (interp x env defs)) args

    StringV ""


