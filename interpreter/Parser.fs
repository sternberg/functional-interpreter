﻿module Parser

open S_Expressions
open Expr

exception Expr_Error of string

let rec sexp_to_expr (se: s_expr) : exprS =
    match se with
    | Atom b when b = "true" || b = "#t" || b = "True" -> BoolS true
    | Atom b when b = "false" || b = "#f" || b = "False" -> BoolS false
    | Atom "empty" -> EmptyS
    | Atom s -> get_int_string_or_id s
    | SList s_exprl ->
        match s_exprl with
        | [ Atom "if"; cond; then'; else' ] -> IfS(sexp_to_expr cond, sexp_to_expr then', sexp_to_expr else')
        | [ Atom "let"; SList head; SList body ] -> LetS(parse_let_head head, sexp_to_expr (SList body))
        | [ Atom "define"; Atom id; expr ] -> DefineS(id, sexp_to_expr expr)
        | [ Atom "define"; SList (Atom fun_name :: parameters); SList body ] ->
            Fun_DefS(fun_name, get_params parameters, sexp_to_expr (SList(body)))
        | [ Atom "lambda"; SList params'; SList body ] -> LambdaS(get_params params', sexp_to_expr (SList(body)))
        | [ Atom op; arg ] -> parse_unary op arg
        | [ Atom op; fst; snd ] -> parse_binary op fst snd
        | Atom op :: args -> parse_variable_args op args
        | e :: args -> Fun_AppS(sexp_to_expr e, get_args args)
        | _ -> raise (Expr_Error("Unable to parse SList"))

and parse_unary op arg =
    match op with
    | "first" -> UOpS(FirstS, sexp_to_expr arg)
    | "rest" -> UOpS(RestS, sexp_to_expr arg)
    | "empty?" -> UOpS(IsEmptyS, sexp_to_expr arg)
    | "++" -> UOpS(IncS, sexp_to_expr arg)
    | "--" -> UOpS(DecS, sexp_to_expr arg)
    | "not" -> UOpS(NotS, sexp_to_expr arg)
    | _ -> parse_variable_args op [ arg ]

and parse_binary op fst snd =
    match op with
    | "link" -> BinopS(LinkS, sexp_to_expr fst, sexp_to_expr snd)
    | "-" -> BinopS(SubS, sexp_to_expr fst, sexp_to_expr snd)
    | "/" -> BinopS(DivS, sexp_to_expr fst, sexp_to_expr snd)
    | ">" -> BinopS(GTS, sexp_to_expr fst, sexp_to_expr snd)
    | "<" -> BinopS(LTS, sexp_to_expr fst, sexp_to_expr snd)
    | ">=" -> greater_less_or_equal_to GTS fst snd
    | "<=" -> greater_less_or_equal_to LTS fst snd
    | "eq?" -> BinopS(EQS, sexp_to_expr fst, sexp_to_expr snd)
    | "modulo" -> BinopS(ModS, sexp_to_expr fst, sexp_to_expr snd)
    | _ -> parse_variable_args op [ fst; snd ]

and greater_less_or_equal_to op fst snd =
    let a = sexp_to_expr fst
    let b = sexp_to_expr snd
    BinopS(OrS, BinopS(EQS, a, b), BinopS(op, a, b))

and parse_variable_args op args =
    match op with
    | "print" -> PrintS(get_args args)
    | "or" -> parse_bool_op OrS args (BoolS(false))
    | "and" -> parse_bool_op AndS args (BoolS(true))
    | "string+" -> BinopS'(StrAppendS, List.map sexp_to_expr args)
    | "+" -> BinopS'(AddS, List.map sexp_to_expr args)
    | "*" -> BinopS'(MulS, List.map sexp_to_expr args)
    | "list" -> parse_list args
    | "cond" -> CondS(parse_cond args)
    | f -> Fun_AppS(IdS f, get_args args)

and get_args l = List.map sexp_to_expr l

and get_params (l: s_expr list) : parameters =
    try
        List.map (fun (Atom f) -> f) l
    with
    | _ -> raise (Expr_Error("Unable to parse parameters."))

and parse_let_head (l: s_expr list) : (id * exprS) list =
    try
        List.map (fun (SList [ Atom x; y ]) -> x, sexp_to_expr y) l
    with
    | _ -> raise (Expr_Error("Unable to parse let bindings."))

/// Kind of pre-desugaring, var arg OR -> nested ORs -> desugarer turns it into nested if then elses
/// AND, OR is not in the core language.
and parse_bool_op op l baseval =
    match l with
    | [] -> baseval
    | x :: [] -> sexp_to_expr x
    | first :: rest -> BinopS(op, sexp_to_expr first, parse_bool_op op rest baseval)

and parse_list (l: s_expr list) : exprS =
    match l with
    | [] -> EmptyS
    | first :: rest -> BinopS(LinkS, sexp_to_expr first, parse_list rest)

and parse_cond cases =
    match cases with
    | [ SList [ Atom "else"; body ] ] -> [ IdS "else", sexp_to_expr body ]
    | SList [ test; body ] :: rest ->
        (sexp_to_expr test, sexp_to_expr body)
        :: parse_cond rest
    | _ -> raise (Expr_Error("Unable to parse cond cases."))

/// TODO: use regex to determine float, int, string, symbol
and get_int_string_or_id s =
    match string_to_int s with
    | Some n -> NumS n
    | None ->
        let len = s.Length in

        if s.[0] = '"' && s.[len - 1] = '"' then
            let s' = s.[1..len - 2].Replace("\s", " ")
            StringS s'
        else
            IdS s

and string_to_int s =
    try
        Some(int s)
    with
    | _ -> None
