﻿module Parser_Typed

open S_Expressions
open Expr
open Expr_Typed

exception Expr_Error_Typed of string

let rec sexp_to_exprT (se: s_expr) : exprST =
    match se with
    | Atom b when b = "true" || b = "#t" || b = "True" -> BoolST true
    | Atom b when b = "false" || b = "#f" || b = "False" -> BoolST false
    | Atom s -> get_int_string_or_id s
    | SList sexps ->
        match sexps with
        | [ Atom "empty"; Atom ":"; Atom type' ] -> EmptyST(get_type [ Atom type' ])
        | Atom "empty" :: Atom ":" :: typ -> EmptyST(get_type typ)
        | [ Atom "if"; cond; then'; else' ] -> IfST(sexp_to_exprT cond, sexp_to_exprT then', sexp_to_exprT else')
        | [ Atom "define"; Atom id; Atom ":"; SList typ; expr ] -> Define_ConstST(id, get_type typ, sexp_to_exprT expr)
        | [ Atom "define"; SList head; Atom ":"; SList fun_type; SList body ] -> parse_fun_def head fun_type body
        | [ Atom "lambda"; SList head; Atom ":"; SList lam_ret; body ] ->
            LambdaST(get_params head, get_param_types head, get_type lam_ret, sexp_to_exprT body)
        | [ Atom op; arg ] -> parse_unaryT op arg
        | [ Atom op; fst; snd ] -> parse_binaryT op fst snd
        | Atom op :: args -> parse_variable_argsT op args
        | e :: args -> Fun_AppST(sexp_to_exprT e, get_argsT args)
        | _ -> raise (Expr_Error_Typed "Unable to parse s-expression.")

and parse_unaryT op arg =
    match op with
    | "first" -> UOpST(FirstS, sexp_to_exprT arg)
    | "rest" -> UOpST(RestS, sexp_to_exprT arg)
    | "empty?" -> UOpST(IsEmptyS, sexp_to_exprT arg)
    | "++" -> UOpST(IncS, sexp_to_exprT arg)
    | "--" -> UOpST(DecS, sexp_to_exprT arg)
    | "not" -> UOpST(NotS, sexp_to_exprT arg)
    | _ -> parse_variable_argsT op [ arg ]

and parse_binaryT op fst snd =
    match op with
    | "link" -> BinopST(LinkS, sexp_to_exprT fst, sexp_to_exprT snd)
    | "-" -> BinopST(SubS, sexp_to_exprT fst, sexp_to_exprT snd)
    | "/" -> BinopST(DivS, sexp_to_exprT fst, sexp_to_exprT snd)
    | ">" -> BinopST(GTS, sexp_to_exprT fst, sexp_to_exprT snd)
    | "<" -> BinopST(LTS, sexp_to_exprT fst, sexp_to_exprT snd)
    | ">=" -> greater_less_or_equal_toT GTS fst snd
    | "<=" -> greater_less_or_equal_toT LTS fst snd
    | "eq?" -> BinopST(EQS, sexp_to_exprT fst, sexp_to_exprT snd)
    | "modulo" -> BinopST(ModS, sexp_to_exprT fst, sexp_to_exprT snd)
    | _ -> parse_variable_argsT op [ fst; snd ]

and greater_less_or_equal_toT op fst snd =
    let a = sexp_to_exprT fst
    let b = sexp_to_exprT snd
    BinopST(OrS, BinopST(EQS, a, b), BinopST(op, a, b))

and parse_variable_argsT op args =
    match op with
    | "or" -> parse_bool_op OrS args (BoolST(false))
    | "and" -> parse_bool_op AndS args (BoolST(true))
    | "append" -> BinopST'(StrAppendS, get_argsT args)
    | "+" -> BinopST'(AddS, get_argsT args)
    | "*" -> BinopST'(MulS, get_argsT args)
    | "list" -> parse_list args
    | "cond" -> CondST(parse_cond args)
    | f -> Fun_AppST(IdST f, get_argsT args)

and get_argsT l = List.map sexp_to_exprT l

and get_int_string_or_id s =
    match string_to_int s with
    | Some n -> NumST n
    | None ->
        let len = s.Length in

        if s.[0] = '"' && s.[len - 1] = '"' then
            let s' = s.[1..len - 2].Replace("\s", " ")
            StringST(s')
        else
            IdST s

and string_to_int s =
    try
        Some(int s)
    with
    | _ -> None

and parse_fun_def head fun_type body =
    match head with
    | Atom f :: rest ->
        let p = get_params rest
        let a = get_param_types rest
        Fun_DefST(f, p, a, get_type fun_type, sexp_to_exprT (SList(body)))
    | _ -> failwith "wrongly formated function def"

and get_params (l: s_expr list) : parameters =
    try
        List.map (fun (SList (Atom x :: _)) -> x) l
    with
    | _ -> raise (Expr_Error_Typed "Unable to parse parameters.")


and get_param_types (l: s_expr list) =
    match l with
    | [] -> []
    | SList (_ :: Atom ":" :: type') :: rest -> get_param_type type' :: get_param_types rest
    | _ -> raise (Expr_Error_Typed "Unable to parse parameter types.")

and get_param_type p =
    match p with
    | [] -> raise (Expr_Error_Typed "Missing type.")
    | _ :: _ when is_fun_type p ->
        let p_before_arrow = get_params_before_arrow p
        let p_after_arrow = get_params_after_arrow p
        Fun(get_types_before_arrow_helper p_before_arrow, get_type p_after_arrow)
    | [ Atom "listof"; Atom _ ] as t -> get_type t
    | [ _ ] as pt -> get_type pt
    | _ -> raise (Expr_Error_Typed "Unable to parse parameter type.")

/// Get the param type up to "->"
/// Example Input: listof num -> num
/// Ouput: listof num
and get_params_before_arrow l =
    let rec aux l acc =
        match l with
        | Atom "->" :: _ -> acc
        | x :: rest -> aux rest (acc @ [ x ])
        | _ -> []

    aux l []

/// Example Input: listof num -> num
/// Ouput: num
and get_params_after_arrow l =
    match l with
    | Atom "->" :: rest -> rest
    | _ :: rest -> get_params_after_arrow rest

/// Ex. num num listof num
/// Output [Num; Num; List(Num)]
/// TODO: robustness
and get_types_before_arrow_helper l : Type list =
    match l with
    | [] -> []
    | [ Atom _ ] as t -> [ get_type t ]
    | Atom "listof" :: Atom "listof" :: Atom x :: rest ->
        get_type [ Atom "listof"
                   Atom "listof"
                   Atom x ]
        :: get_types_before_arrow_helper rest
    | Atom "listof" :: Atom x :: rest ->
        get_type [ Atom "listof"; Atom x ]
        :: get_types_before_arrow_helper rest
    | Atom x :: rest ->
        get_type ([ Atom x ])
        :: get_types_before_arrow_helper rest

and is_fun_type (l: s_expr list) =
    List.foldBack (fun x acc -> acc || x = Atom "->") l false

and get_type (t: s_expr list) =
    match t with
    | [ Atom "num" ] -> Num
    | [ Atom "bool" ] -> Bool
    | [ Atom "string" ] -> String
    | Atom "listof" :: x -> List(get_type x)
    | _ -> raise (Expr_Error_Typed "Unable to parse type.")

and parse_cond cases =
    match cases with
    | [ SList [ Atom "else"; body ] ] -> [ IdST "else", (sexp_to_exprT body) ]
    | SList [ test; body ] :: rest ->
        (sexp_to_exprT test, sexp_to_exprT body)
        :: parse_cond rest
    | _ -> []

and parse_list (l: s_expr list) : exprST =
    match l with
    | first :: _ ->
        // Have to first find the type so we know what type EmpyST will be
        let t =
            match sexp_to_exprT first with
            | NumST n -> Num
            | StringST s -> String
            | BoolST t -> Bool
            | _ -> raise (Expr_Error_Typed "Unable to list type.")

        let rec aux ll =
            match ll with
            | [] -> EmptyST t
            | first :: rest -> BinopST(LinkS, sexp_to_exprT first, aux rest)

        aux l

/// Kind of pre-desugaring, var arg OR -> nested ORs -> desugarer turns it into nested if thens
/// AND, OR is not in the core language.
and parse_bool_op op l baseval =
    match l with
    | [] -> baseval
    | x :: [] -> sexp_to_exprT x
    | first :: rest -> BinopST(op, sexp_to_exprT first, parse_bool_op op rest baseval)
