﻿module REPL

open System
open System.IO
open System.Text

open S_Expressions
open Parser
open Expr
open Interp
open Desugarer
open Parser_Typed
open Desugar_Typed
open Type_Checker
open Expr_Typed
open REPL_Output

let rec get_input acc =
    printf
        "%s"
        (if String.IsNullOrEmpty acc then
             " > "
         else
             "... ")

    let msg = Console.ReadLine()

    if String.IsNullOrEmpty msg then
        get_input acc
    else
        match msg.[msg.Length - 1] with
        | ';' ->
            if msg.Length = 1 then
                acc
            else
                acc + msg.[0..msg.Length - 2]
        | _ -> get_input (acc + " " + msg)

let parse_typed s =
    s |> string_to_sexp |> sexp_to_exprT |> desugarT

let parse s =
    s |> string_to_sexp |> sexp_to_expr |> desugar


/// Main repl for type checker
let rec repl_typed (defs: tenv) =
    try
        match type_check (parse_typed (get_input "")) defs with
        | Tenv (v, id, t) ->
            printf " %s: %s\n" id (pretty_types t)
            repl_typed v
        | v -> printf " %s\n" (pretty_types v)
    with
    | e -> error_msg e

    repl_typed defs


let rec repl_debug (defs: defs) =
    try
        let sexp = (get_input "") |> string_to_sexp

        printf "S-Expr: %A\n" sexp
        let expr = sexp |> sexp_to_expr

        printf "Expr: %A\n" expr
        let deExpr = expr |> desugar
        printf "Desugared: %A\n" deExpr

        match interpret deExpr defs with
        | Venv (env, id, v) ->
            printf " %s: %s\n" id (pretty_value v)
            repl_debug env
        | v -> printf "%s\n" (pretty_value v)
    with
    | e -> error_msg e

    repl_debug defs


let rec repl (defs: defs) =
    try
        match interpret (parse (get_input "")) defs with
        | Venv (env, id, v) ->
            printf " %s: %s\n" id (pretty_value v)
            repl env
        | v -> printf " %s\n" (pretty_value v)
    with
    | e -> error_msg e

    repl defs

let mt_venv: venv = Map.empty<string, value>
let mt_tenv: tenv = Map.empty<string, Type>

let rec choose_mode () =
    printf
        "Menu: \n1: REPL\n2: REPL Debug Mode\n3: REPL Type Checking\n\
    4: Read from file\n5: Run Test Suite\n\
    Once in the REPL, end line with ';' to evaluate.\n\n > "

    match Console.ReadLine() with
    | "1" -> repl mt_venv
    | "2" -> repl_debug mt_venv
    | "3" -> repl_typed mt_tenv
    | "4" -> read_from_file ()
    | "5" -> run_test_suite ()
    | _ -> choose_mode ()

and read_from_file () =
    printf "1: Untyped\n2: Typed\n\n > "

    match Console.ReadLine() with
    | "1" ->
        match load_file () with
        | Some file_list -> load_file_definitions file_list mt_venv
        | None ->
            printf "Unable to open file.\n"
            choose_mode ()
    | "2" ->
        match load_file () with
        | Some file_list -> load_file_definitions_typed file_list mt_tenv
        | None ->
            printf "Unable to open file.\n"
            choose_mode ()
    | _ -> read_from_file ()

and load_file () =
    printf "Enter filename: "

    match get_file (Console.ReadLine()) with
    | Some (file: string) -> Some(file.Split(";") |> List.ofSeq)
    | None -> None

and get_file path =
    try
        Some(File.ReadAllText(path))
    with
    | _ -> None

and load_file_definitions l defs =
    match l with
    | [] -> repl defs
    | first :: rest ->
        try
            match interpret (parse first) defs with
            | Venv (env, id, v) ->
                printf " %s: %s\n" id (pretty_value v)
                load_file_definitions rest env
            | v -> printf " %s\n" (pretty_value v)
        with
        | e -> error_msg e

        load_file_definitions rest defs

and load_file_definitions_typed l (defs: tenv) =
    match l with
    | [] -> repl_typed defs
    | first :: rest ->
        try
            match type_check (parse_typed first) defs with
            | Tenv (v, id, t) ->
                printf " %s: %s\n" id (pretty_types t)
                load_file_definitions_typed rest v
            | v -> printf " %s\n" (pretty_types v)

        with
        | e -> error_msg e

        load_file_definitions_typed rest defs

and run_test_suite () =
    Interpreter.Test.all ()
    choose_mode ()
