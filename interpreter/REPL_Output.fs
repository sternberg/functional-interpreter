﻿module REPL_Output

open S_Expressions
open Parser
open Expr
open Interp
open Desugarer
open Parser_Typed
open Desugar_Typed
open Type_Checker
open Expr_Typed

let rec print_params p =
    match p with
    | [] -> ""
    | [p] -> p + ")"
    | first::rest -> first + " " + print_params rest

let rec pretty_value v =
    match v with
    | NumV n -> string n
    | BoolV b -> string b
    | StringV s -> s
    | EmptyV -> "Empty"
    | LinkV (_, _) as l -> pretty_list l
    | ClosureV (parameters, _, _) -> "(" + print_params parameters

and pretty_list l =
    let rec pp ll =
        match ll with
        | EmptyV -> "]"
        | LinkV (f, EmptyV) -> pretty_value f + "]"
        | LinkV (f, r) -> pretty_value f + "; " + pp r

    "[" + pp l

let rec print_param_types p =
    match p with
    | [] -> ""
    | [ first ] -> pretty_types first
    | first :: rest -> pretty_types first + " " + print_param_types rest

and pretty_types t =
    match t with
    | Num -> "Number"
    | Bool -> "Boolean"
    | String -> "String"
    | Fun (param_types, return_type) -> sprintf "(%s -> %s)" (print_param_types param_types) (pretty_types return_type)
    | List t -> sprintf "List of %s" (pretty_types t)

let val_to_string v =
    match v with
    | NumV n -> "Number"
    | BoolV b -> "Bool"
    | StringV s -> "String"
    | EmptyV -> "Empty"
    | LinkV (_, _) as l -> "List"
    | ClosureV (_, _, _) -> "Closure"

let op_to_string op =
    match op with
    | EQ -> "eq?"
    | GT -> ">"
    | LT -> "<"
    | Add -> "+"
    | Mul -> "*"
    | Div -> "/"
    | Sub -> "-"
    | Mod -> "modulo"
    | Link -> "Link"
    | StrAppend -> "append"

let error_msg e =
    match e with
    | SexprError e -> printf "%s\n" e
    | Expr_Error e -> printf "%s\n" e
    | Desugar_Error e -> printf "%s\n" e
    | Interp_Binop_Error (op, v1, v2) ->
        printf "Type mismatch for %s. Got: %s %s.\n" (op_to_string op) (val_to_string v1) (val_to_string v2)
    | Interp_Binop_Error_Div (v1, v2) ->
        printf "Division by zero exception. Got %s %s.\n" (pretty_value v1) (pretty_value v2)
    | Interp_Unary_Error (op, v1) -> printf "Type mismatch for %A. Got %s.\n" op (pretty_value v1)
    | Interp_Unbound_Id id -> printf "Unbound identifier: %s\n" id
    | Interp_If_Then_Error e -> printf "%s\n" e
    | Interp_Fun_App_Error e -> printf "%s\n" e
    | Expr_Error_Typed e -> printf "%s\n" e
    | Desugar_Error_Typed e -> printf "%s\n" e
    | Binop_Type_Error (op, t1, t2) ->
        printf "Type mismatch for %A. Expected Got %s %s.\n" op (pretty_types t1) (pretty_types t2)
    | Unary_Type_Error (op, t1) -> printf "Type mismatch for %A: Got %s.\n" op (pretty_types t1)
    | Unbound_Id id -> printf "Unbound identifier: %s\n" id
    | IF_Cond_Type_Error (t) -> printf "Type mismatch for if condition: Got %s.\n" (pretty_types t)
    | If_Then_Else_Type_Error (t1, t2) ->
        printf "Type mismatch for if then else branches. Got %s %s.\n" (pretty_types t1) (pretty_types t2)
    | Fun_App_Error e -> printf "%s\n" e
    | Fun_App_Param_Mismatch(t1, t2) -> 
        printf "Type mismatch for function application. Expected %s, got %s.\n" (pretty_types t1) (pretty_types t2)
    | Type_Error e -> printf "%s\n" e
    | _ -> printf "Misc error.\n"
