﻿module S_Expressions

open System.Collections.Immutable

/// Symbolic Expressions
type s_expr =
    | Atom of string
    | SList of s_expr list

exception SexprError of string

let is_whitespace c = System.Char.IsWhiteSpace c

let is_atom_char c =
    not (is_whitespace c) && c <> ')' && c <> ']'

let rec lex_atom (str, rest) =
    match rest with
    | c :: rest' when is_atom_char c -> lex_atom (str + string c, rest')
    | _ -> (str, rest)

let add_to_slist (sl: s_expr) (a: s_expr) =
    match sl with
    | SList x -> SList(x @ [ a ])
    | _ -> raise (SexprError("adding to non slist"))

let string_to_sexpr (str: char list) : s_expr =
    let stack = ImmutableStack<s_expr>.Empty

    let rec aux (cl: char list) (st: ImmutableStack<s_expr>) : s_expr =
        match cl with
        | [] -> st.Peek()
        | c :: rest when is_whitespace c -> aux rest st
        | '[' :: rest
        | '(' :: rest -> aux rest (st.Push(SList []))
        | ']' :: rest
        | ')' :: rest ->
            if st.IsEmpty then
                raise (SexprError("Missing left paren."))
            else
                let top = st.Peek()
                let st' = st.Pop()

                if st'.IsEmpty then
                    top
                else
                    let top2 = st'.Peek()
                    let st'' = st'.Pop()
                    aux rest (st''.Push(add_to_slist top2 top))
        | c :: rest ->
            let s, rest' = lex_atom (string c, rest)

            if st.IsEmpty then
                Atom s
            else
                let top = st.Peek()
                let st' = st.Pop()
                aux rest' (st'.Push(add_to_slist top (Atom s)))

    aux str stack

let string_to_sexp (s: string) : s_expr = string_to_sexpr (List.ofSeq s)    
