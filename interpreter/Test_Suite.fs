﻿namespace Interpreter


open S_Expressions
open Parser
open Expr
open Desugarer
open Interp
open Parser_Typed
open Desugar_Typed
open Type_Checker
open Expr_Typed

[<RequireQualifiedAccess>]
module Test =
    open FsCheck
    open global.Xunit

    let mt_venv = Map.empty<string, value>
    let mt_tenv = Map.empty<string, Type>

    let rnd = new System.Random()
    let binops_array = [| "+"; "-"; "*"; "/" |]
    let random_binop () = binops_array.[ rnd.Next(4) ]

    type ``S-Expressions`` =

        static member ``1`` (x: int) (y: int) =
            let t1 = $"(+ {y} {x})"

            string_to_sexp t1 = SList [ Atom "+"
                                        Atom $"{y}"
                                        Atom $"{x}" ]

        static member ``2``(x: int) =
            let b = random_binop ()
            let t1 = $"({b} 1 {x})"

            string_to_sexp t1 = SList [ Atom $"{b}"
                                        Atom "1"
                                        Atom $"{x}" ]

    let string_to_expr s =
        s |> string_to_sexp |> sexp_to_expr |> desugar

    let interp_test_num s fds =
        match interp (string_to_expr s) mt_venv fds with
        | NumV n -> n
        | _ -> 0

    let interp_test_str s fds =
        match interp (string_to_expr s) mt_venv fds with
        | StringV s -> s
        | _ -> "w/e"

    let interp_test_bool s fds =
        match interp (string_to_expr s) mt_venv fds with
        | BoolV b -> b
        | _ -> false

    let interp_test_list s fds =
        match interp (string_to_expr s) mt_venv fds with
        | LinkV (_, _) as l -> l
        | _ -> EmptyV

    let load_function f fds =
        match interp (string_to_expr f) mt_venv fds with
        | Venv(v,_, _) -> v
        | _ -> mt_venv

    let list_to_string l =
        let rec aux acc ll =
            match ll with
            | [] -> acc
            | x :: xs -> aux (acc + (" " + string x)) xs
        aux "" l

    let list_to_list_string (l) =
        "(list" + list_to_string l + ")"

    let rec value_to_fsharp_list_num l =
        match l with
        | EmptyV -> []
        | LinkV (NumV f, r) -> f :: value_to_fsharp_list_num r
        | _ -> []

    let rec value_to_fsharp_list_bool l =
        match l with
        | EmptyV -> []
        | LinkV (BoolV f, r) -> f :: value_to_fsharp_list_bool r
        | _ -> []

    /// Here we succesively add new definitions to environments
    let len_s =
        "(define (len l)
           (cond
             [(empty? l) 0]
             [else (+ 1 (len (rest l)))]))"
    let len_env = load_function len_s mt_venv

    let filter_s =
        "(define (filter p l)
           (cond
             [(empty? l) empty]
             [(p (first l))
               (link (first l) (filter p (rest l)))]
             [else
               (filter p (rest l))]))"
    let filter_env = load_function filter_s len_env

    let map_s =
        "(define (map f l)
           (cond
             [(empty? l) empty]
             [else
               (link (f (first l)) (map f (rest l)))]))"
    let map_env = load_function map_s filter_env

    let even_s =
        "(define (even? x) (eq? 0 (modulo x 2)))"
    let even_env = load_function even_s map_env

    let odd_s = "(define (odd? x) (not (even? x)))"
    let odd_env = load_function odd_s even_env

    let member_s =
        "(define (member? x l)
           (cond
             [(empty? l) #f]
             [(eq? x (first l)) #t]
             [else (member? x (rest l))]))"
    let member_env = load_function member_s odd_env

    let foldr_s =
        "(define (foldr f init l)
           (cond
             [(empty? l) init]
             [else
               (f (first l)
                  (foldr f init (rest l)))]))"
    let foldr_env = load_function foldr_s member_env

    let plus_s = "(define (plus x y) (+ x y))"
    let plus_env = load_function plus_s foldr_env

    let list_sum_s = "(define (list-sum l) (foldr plus 0 l))"
    let list_sum_env = load_function list_sum_s plus_env

    let foldl_s =
        "(define (foldl f acc l)
           (cond
             [(empty? l) acc]
             [else
               (foldl f
                      (f (first l) acc)
                         (rest l))]))"
    let foldl_env = load_function foldl_s list_sum_env

    let list_sum_s_left_fold = "(define (list-sumL l) (foldl (lambda (a b) (+ a b)) 0 l))"
    let list_sum_env' = load_function list_sum_s_left_fold foldl_env

    let concat_s = 
        "(define (concat l r)
           (cond
             [(empty? l) r]
             [else
               (link (first l) (concat (rest l) r))]))"
    let concat_env = load_function concat_s list_sum_env'

    let distinct_s = 
        "(define (distinct l)
           (cond
             [(empty? l) empty]
             [(member? (first l) (rest l))
               (distinct (rest l))]
             [else (link (first l) (distinct (rest l)))]))"
    let distinct_env = load_function distinct_s concat_env

    let rev_s1 = 
        "(define (rev1 l)
           (cond
             [(empty? l) l]
             [else (concat (rev1 (rest l)) (list (first l)))]))"
    let rev_env1 = load_function rev_s1 distinct_env

    let cons_s = 
        "(define (cons a b) (link a b))"
    let cons_env = load_function cons_s rev_env1

    let rev_s = 
        "(define (reverse l) (foldl cons empty l))"
    let rev_env = load_function rev_s cons_env

    let minus_s =
        "(define (minus a b) (- a b))"
    let minus_env = load_function minus_s rev_env

    let fac_s = "(define (fac x) (if (eq? x 0) 1 (* x (fac (- x 1)))))"
    let fac_env = load_function fac_s rev_env 
    let rec fac_fsharp x = if x = 0 then 1 else x * fac_fsharp (x - 1)

    type ``Interpreter evaluation`` =
        static member ``x + 1``(x: int) =
            let t1 = $"(+ {x} 1)"
            interp_test_num t1 mt_venv = x + 1

        static member ``x + 1 lam``(x: int) =
            let t1 = $"((lambda (x) (+ x 1)) {x})"
            interp_test_num t1 mt_venv = x + 1

        static member ``Var arg +``(x: int list) =
            let l = list_to_string x
            let t1 = $"(+ {l})"
            interp_test_num t1 mt_venv = List.sum x

        static member ``Fac``(x: int ) =
            let x' = abs x
            let t1 = $"(fac {x'})"
            interp_test_num t1 fac_env = fac_fsharp x'

        static member ``Var arg *``(x: int list) =
            let l = list_to_string x
            let t1 = $"(* {l})"
            interp_test_num t1 mt_venv = List.fold (fun a b -> a * b) 1 x

        static member ``let x + y`` (x: int) (y: int) =
            let t = $"(let ([a {x}]) (let ([b {y}]) (+ a b)))"
            let a = x in
            let b = y in
            a + b = interp_test_num t mt_venv

        static member ``list length``(x: int list) =
            let t = list_to_list_string x
            let a = interp_test_num $"(len {t})" len_env
            a = List.length x

        static member ``filter even?``(x: int list) =
            let l = list_to_list_string x
            let t = $"(filter even? {l}))"
            let a = interp_test_list t even_env
            let b = value_to_fsharp_list_num a
            b = (List.filter (fun x -> x % 2 = 0) x)

        static member ``list length + filter``(x: int list) =
            let l = list_to_list_string x
            let t = $"(len (filter (lambda (x) (eq? 0 (modulo x 2))) {l}))"
            interp_test_num t filter_env = List.length (List.filter (fun x -> x % 2 = 0) x)

        static member ``map 1`` (x: int list) (y: int) =
            let l = list_to_list_string x
            let t = $"(map (lambda (x) (+ x {y})) {l}))"
            let a = interp_test_list t map_env
            let b = value_to_fsharp_list_num a
            b = List.map (fun a -> a + y) x

        static member ``member?`` (x: int list) (y: int) =
            let l = list_to_list_string x
            let t = $"(member? {y} {l})"
            let a = interp_test_bool t member_env
            a = List.contains y x

        static member ``List sum``(x: int list) =
            let l = list_to_list_string x
            let t = $"(list-sum {l})"
            let a = interp_test_num t list_sum_env
            a = List.sum x

        static member ``List sum: left vs right fold``(x: int list) =
            let l = list_to_list_string x
            let t = $"(list-sum {l})"
            let r = $"(list-sumL {l})"
            let a = interp_test_num t list_sum_env
            let b = interp_test_num r list_sum_env'
            let c = List.sum x
            a = b && a = c

        static member ``List concatenation`` (x: int list) (y: int list) =
            let l = list_to_list_string x
            let r = list_to_list_string y
            let r = $"(concat {l} {r})"
            let a = interp_test_list r concat_env
            let res = value_to_fsharp_list_num a
            List.length res = List.length (x @ y)
             
        static member ``List distinct`` (x: int list) =
            let l = list_to_list_string x
            let r = $"(distinct {l})"
            let a = interp_test_list r distinct_env
            let res = value_to_fsharp_list_num a
            List.length res = List.length (List.distinct x)

        static member ``List rev and foldl minus`` (x: int list) =
            let l = list_to_list_string x
            let r = $"(foldr minus 0 (reverse {l}))"
            let a = interp_test_num r minus_env
            a = List.foldBack (-) (List.rev x) 0


    type ``Booleans`` = 
        static member ``And var arg bool `` (x: bool list) =
            let l = list_to_string x  
            let r = $"(and {l})"
            let a = interp_test_bool r mt_venv
            a = List.fold (fun a b -> a && b) true x

        static member ``Or var arg bool `` (x: bool list) =
            let l = list_to_string x
            let r = $"(or {l})"
            let a = interp_test_bool r mt_venv
            a = List.fold (fun a b -> a || b) false x

        static member ``Filter true`` (x: bool list) =
            let l = list_to_list_string x
            let r = $"(filter (lambda (x) (if x true false)) {l})"
            let a = interp_test_list r distinct_env
            let res = value_to_fsharp_list_bool a
            List.length res = List.length (List.filter (fun a -> a = true) x)

        static member ``Map not -> Filter true`` (x: bool list) =
            let l = list_to_list_string x
            let r = $"(filter (lambda (x) (if x true false)) (map (lambda (x) (not x)) {l}))"
            let a = interp_test_list r distinct_env
            let res = value_to_fsharp_list_bool a
            let x' = List.map (fun a -> not a) x
            List.length res = List.length (List.filter (fun a -> a = true) x')

    // Type Checker

    let string_to_exprT s =
        s |> string_to_sexp |> sexp_to_exprT |> desugarT

    let load_function_typed f (fds: tenv) =
        match type_check (string_to_exprT f) fds with
        | Tenv(v, id, t) -> v
        | _ -> failwith "t"

    let tc_test s tenv =
        let a = string_to_exprT s
        type_check a tenv

    let fac_st = "(define (fac [x : num]) : [num] (if (eq? x 0) 1 (* x (fac (- x 1)))))"
    let fac_tenv = load_function_typed fac_st mt_tenv

    type ``Type Checking`` = 
        static member ``Type Check Recursive: Fac`` (x: int) =
            let t = $"(fac {x})"
            tc_test t fac_tenv = Num 
            

    let all () =
        Check.QuickAll<``S-Expressions``>()
        Check.QuickAll<``Interpreter evaluation``>()
        Check.QuickAll<``Type Checking``>()
        Check.QuickAll<``Booleans``>()
