﻿module Type_Checker

open Expr
open Expr_Typed

exception Binop_Type_Error of binary_op * Type * Type
exception Unary_Type_Error of unary_op * Type
exception Unbound_Id of string
exception IF_Cond_Type_Error of Type
exception If_Then_Else_Type_Error of Type * Type
exception Fun_App_Error of string
// Expected * Got
exception Fun_App_Param_Mismatch of Type * Type
exception Type_Error of string

let tc_binop_arith op (l: Type) (r: Type) : Type =
    match l, r with
    | Num, Num -> Num
    | _ -> raise (Binop_Type_Error(op, l, r))

let tc_val_compare op (l: Type) (r: Type) : Type =
    match l, r with
    | Num, Num -> Bool
    | String, String -> Bool
    | _ -> raise (Binop_Type_Error(op, l, r))

let str_append (l: Type) (r: Type) : Type =
    match l, r with
    | String, String -> String
    | _ -> raise (Binop_Type_Error(StrAppend, l, r))

let tc_is_empty (l: Type) =
    match l with
    | List _ -> Bool
    | _ -> raise (Unary_Type_Error(IsEmpty, l))

let tc_get_rest (l: Type) : Type =
    match l with
    | List t -> List t
    | _ -> raise (Unary_Type_Error(Rest, l))

let tc_get_first (l: Type) : Type =
    match l with
    | List t -> t
    | _ -> raise (Unary_Type_Error(First, l))

let tc_link (l: Type) (r: Type) : Type =
    match l, r with
    | t1, List t2 when t1 = t2 -> List t2
    | _ -> raise (Binop_Type_Error(Link, l, r))

let mt_tenv = Map.empty<string, Type>

let rec type_of (x: string) (env: tenv) (fds: tenv) : Type =
    match fds.TryFind(x), env.TryFind(x) with
    | None, None -> raise (Unbound_Id x)
    | None, Some s -> s
    | Some s, None -> s
    | Some _, Some a -> a

/// Wrapper for tc, always call with empty type environment.
and type_check expr fds = tc expr mt_tenv fds

/// Main type checking function.
and tc (expr: exprT) (env: tenv) (fds: tenv) : Type =
    match expr with
    | NumT _ -> Num
    | BoolT _ -> Bool
    | StringT _ -> String
    | IdT s -> type_of s env fds
    | EmptyT t -> List t
    | UOpT (uop, fst) -> tc_uop uop fst env fds
    | BinopT (bop, fst, snd) -> type_check_bop bop fst snd env fds
    | IfT (cond, then', else') -> type_check_if cond then' else' env fds
    | Define_ConstT (id, typ, expr) -> type_check_const id typ expr env fds
    | DefineT (id, expr) -> type_check_define id expr env fds
    | LambdaT (parameters, param_types, ret_t, body) -> tc_lambda parameters param_types ret_t body env fds
    | Fun_AppT (f, args) -> type_check_function_application f args env fds

and tc_uop uop expr env fds : Type =
    match uop, tc expr env fds with
    | First, l -> tc_get_first l
    | Rest, l -> tc_get_rest l
    | IsEmpty, l -> tc_is_empty l

and type_check_bop (bop: binary_op) (expr1: exprT) (expr2: exprT) (env: tenv) fds : Type =
    match bop, tc expr1 env fds, tc expr2 env fds with
    | Add, fst, snd -> tc_binop_arith bop fst snd
    | Sub, fst, snd -> tc_binop_arith bop fst snd
    | Mul, fst, snd -> tc_binop_arith bop fst snd
    | Div, fst, snd -> tc_binop_arith bop fst snd
    | Mod, fst, snd -> tc_binop_arith bop fst snd
    | EQ, fst, snd -> tc_val_compare bop fst snd
    | GT, fst, snd -> tc_val_compare bop fst snd
    | LT, fst, snd -> tc_val_compare bop fst snd
    | StrAppend, fst, snd -> str_append fst snd
    | Link, fst, snd -> tc_link fst snd

and type_check_if cond thn els env fds =
    match tc cond env fds with
    | Bool ->
        match tc thn env fds, tc els env fds with
        | t1, t2 when t1 = t2 -> t1
        | t1, t2 -> raise (If_Then_Else_Type_Error(t1, t2))
    | t -> raise (IF_Cond_Type_Error(t))

and type_check_define id expr env fds =
    match expr with
    | LambdaT (parameters, param_types, ret_t, body) ->
        // For recursive functions we first have to bind the function name to Fun type
        let f = Fun(param_types, ret_t)
        let newenv = env.Add(id, f)
        // Next we bind all of the parameters to their type
        let paramsbound =
            bind_params parameters param_types newenv fds
        // Finally we type check the body and see if it matches the return type
        match tc body paramsbound fds, ret_t with
        | t1, t2 when t1 = t2 -> Tenv(fds.Add(id, f), id, f) // return to repl with added def
        | t1, t2 -> raise (Type_Error "Type mismatch for function. Return type did not match body.")
    | _ -> raise (Type_Error "Shouldn't be reached.")

and type_check_function_application f args env fds =
    match tc f env fds with
    | Fun (param_types, ret_t) -> /// now we have to make sure the corresponding formal type is eq to arg type.
        match tc_funapp param_types args env fds with
        | true -> ret_t
        | false -> raise (Type_Error "Type mismatch for function application.")
    | _ -> raise (Type_Error "Type mismatch for function application. Not a function")

/// Ex. we have map (paramtypes are : num->num, listof num, and then our args are f and some list of num.
/// the corresponding paramtypes have to match the arg types.
and tc_funapp (param_types: param_types) (args: exprT list) env fds =
    match param_types, args with
    | [], [] -> true
    | [], _
    | _, [] -> raise (Type_Error "Function application recieved wrong number of arguments.")
    // If the param is a fun, the arg better be a fun
    | Fun (ps, _) :: rest, arg_t :: arg_rest ->
        match tc arg_t env fds with
        | Fun (pt, ret_t) ->
            let a = (check_matching_formals ps pt)
            a && tc_funapp rest arg_rest env fds
        | _ -> raise (Type_Error "Function application error. Expected a function as argument.")
    | t :: rest, x :: xs ->
        match t, tc x env fds with
        | t1, t2 when t1 = t2 -> tc_funapp rest xs env fds
        | t1, t2 -> raise (Fun_App_Param_Mismatch(t1, t2))

and check_matching_formals f1 f2 =
    match f1, f2 with
    | [], [] -> true
    | t1 :: rest, t2 :: rest2 when t1 = t2 -> check_matching_formals rest rest2
    | t1 :: _, t2 :: _ -> raise (Fun_App_Param_Mismatch(t1, t2))
    | _ -> raise (Type_Error "Function application. Wrong number of parameters.")

and bind_params (parameters: parameters) (pt: param_types) (env: tenv) fds =
    match parameters, pt with
    | [], [] -> env
    | [], _
    | _, [] -> raise (Type_Error "Param count not the same length as Param type count.")
    | s :: xs, t :: ys -> bind_params xs ys (env.Add(s, t)) fds

and tc_lambda parameters param_types ret_t body env fds =
    let tenv' =
        bind_params parameters param_types env fds

    match tc body tenv' fds, ret_t with
    | t1, ret when t1 = ret -> Fun(param_types, ret_t)
    | _ -> raise (Type_Error "Type mismatch for lambda. Return type did not match body.")

and type_check_const id typ expr env fds =
    match tc expr env fds, typ with
    | t1, t2 when t1 = t2 -> Tenv(fds.Add(id, t2), id, t2)
    | t1, t2 -> raise (Type_Error "Type mismatch for define. Return type did not match body.")
