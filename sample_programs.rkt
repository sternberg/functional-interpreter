#lang racket
;
(define a (let ([g
      (lambda (x y z) (+ x y z))])
  (g 2 3 4)))
;
(let ([x 3] [y 3])
  (+ x y))
;
(let ([x 1] [y 3])
  (let ([x 2])
    (+ x y)))
;
(let ([x ((lambda (x) (+ x 1)) 1)])
      (+ x 1))
;
(define (link f r)
  (cons f r))
;
(define (sum-list l)
  (if (empty? l)
      0
      (+ (first l) (sum-list (rest l)))))
;
(define (len l)
  (cond
    [(empty? l) 0]
    [else (+ 1 (len (rest l)))]))
;
(define (map f l)
  (cond
    [(empty? l) empty]
    [else
     (link (f (first l)) (map f (rest l)))]))
;
(define (filter p l)
  (cond
    [(empty? l) empty]
    [(p (first l))
     (link (first l)(filter p (rest l)))]
    [else
     (filter p (rest l))]))
;
(define (even? x) (eq? 0 (modulo x 2)))  
;
(define (odd? x) (not (even? x)))
;
(define list-1 (list 1 2 3 4 5 6 7 8 9 10))
;
(define (add1 x) (+ x 1))
;
(define list-2 (map add1 list-1))
;
(define list-3 (map (lambda (x) (+ x 1)) list-1))
;
(define even-list (filter even? list-1))
;
(define list-4 (filter (lambda (x) (eq? 0 (modulo x 2))) list-1))
;
(define (member? x l)
  (cond
    [(empty? l) #f]
    [(eq? x (first l)) #t]
    [else (member? x (rest l))]))
;
(define (plus x y) (+ x y))
;
(define (foldr f init l)
  (cond
    [(empty? l) init]
    [else
     (f (first l)
        (foldr f init (rest l)))]))
;
(define (foldl f acc l)
  (cond
    [(empty? l) acc]
    [else
     (foldl f
            (f (first l) acc)
            (rest l))]))
;
(define (list-sum l) (foldl plus 0 l))
;
(foldl (lambda (x y) (+ x y)) 0 (list 1 2 3))
;
(define (concat l r)
  (cond
    [(empty? l) r]
    [else
     (link (first l) (concat (rest l) r))]))
;
(define (distinct l)
  (cond
    [(empty? l) empty]
    [(member? (first l) (rest l))
     (distinct (rest l))]
    [else (link (first l) (distinct (rest l)))]))
;
(define true? (lambda (x) (if x #t #f)))
;
(define (rev1 l)
  (cond
    [(empty? l) l]
    [else (concat (rev1 (rest l)) (list (first l)))]))
;
(define (cons1 a b) (link a b))
;
(define (reverse l) (foldl cons1 empty l))
;
(define (minus a b) (- a b))
;
(define (d l) (foldr minus 0 (reverse l)))
